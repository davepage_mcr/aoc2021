#!/usr/bin/env python3

import argparse
import numpy

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

########################
## Musings start here ##
########################

# This doesn't fit in my brain nicely, ho hum
# I'm going to use lower-case to describe the signals
# and upper-case to describe the segments they light:
#
#  AAAA
# B    C
# B    C
#  DDDD
# E    F
# E    F
#  GGGG

# 2 signals: '1'
# 3 signals: '7'
# 4 signals: '4'
# 5 signals: '2', '3', '5'
# 6 signals: '0', '6', '9'
# 7 signals: '8'

# Useful deductions:
# - If we spot a '1' and a '7' we can figure out which signal corresponds to A

#######################
## Logic starts here ##
#######################

inputfile = open( args.inputfile )

easyspots = 0
for line in inputfile:
    patterns = line.split()
    outpatterns = patterns[-4:]

    for p in outpatterns:
        if len(p) == 2 or len(p) == 3 or len(p) == 4 or len(p) == 7:
            easyspots += 1

print(f"There are {easyspots} instances of '1', '4', '7', '8' in the output values")
