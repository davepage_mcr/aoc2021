#!/usr/bin/env python3

import argparse
import numpy

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

#######################
## Logic starts here ##
#######################

horizon = 20 # recreate part 1 first

# Doing this recursively for 300 fish over 256 days is too fricking slow
# Instead, think about 1 fish that is spawned on day 0 and its offspring with that 256 day horizon
# This gives us a single pattern which we can shift and apply linearly 300 times

onefish = [ 0 ] * horizon

# Now apply this to the initial state

inputfile = open( args.inputfile )
initstate = [ int(x) for x in inputfile.readline().rstrip().split(',') ]
