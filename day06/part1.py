#!/usr/bin/env python3

import argparse
import numpy

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

#######################
## Logic starts here ##
#######################

inputfile = open( args.inputfile )
initstate = [ int(x) for x in inputfile.readline().rstrip().split(',') ]

day = 0
num_fish = len(initstate)
# This array keeps track of how many extra fish we have on each day
extra_fish = numpy.zeros(81, dtype="int")

def add_fish(on_day):
    while on_day < len(extra_fish):
        extra_fish[on_day] += 1
        # Now that new fish will spawn 9 days later!
        add_fish( on_day+9 ) 
        on_day += 7

print("Parsing initial state")
for fish in sorted(initstate):
    add_fish(fish+1)

print(extra_fish)

for day in range(len(extra_fish)):
    num_fish += extra_fish[day]
    print(f"On day {day} we will have {num_fish} fish")
