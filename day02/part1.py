#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

depth = 0
along = 0

inputfile = open( args.inputfile )
for line in inputfile:
    ( direction, distance ) = line.split()
    distance = int(distance)

    if direction == 'forward':
        along += distance
    elif direction == 'down':
        depth += distance
    elif direction == 'up':
        depth -= distance
    else:
        print(f'Unknown direction {direction}')
        break

print(f'Along {along}, depth {depth}, product {along * depth}')
