#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

depth = 0
along = 0
aim = 0

inputfile = open( args.inputfile )
for line in inputfile:
    ( direction, distance ) = line.split()
    distance = int(distance)

    if direction == 'down':
        aim += distance
    elif direction == 'up':
        aim -= distance
    elif direction == 'forward':
        along += distance
        depth += aim * distance
    else:
        print(f'Unknown direction {direction}')
        break

print(f'Along {along}, depth {depth}, product {along * depth}')
