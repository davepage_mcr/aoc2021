#!/usr/bin/env python3

import argparse
import numpy

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

#######################
## Logic starts here ##
#######################

inputfile = open( args.inputfile )
crabs = sorted([ int(x) for x in inputfile.readline().rstrip().split(',') ])

mincost = None
mintarget = None

def fuelcost(distance):
    return int( distance * ( distance + 1 ) / 2 )

for target in range(crabs[0],crabs[-1]+1):
    cost = 0
    for crab in crabs:
        cost += fuelcost ( abs(crab-target) )
    print(f"Fuel cost for target {target}: {cost}")

    if mincost == None or cost < mincost:
        mincost = cost
        mintarget = target

print(f"Minimum fuel cost is {mincost}, at position {mintarget}")
