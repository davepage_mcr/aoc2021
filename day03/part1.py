#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

inputfile = open( args.inputfile )
report = [ l.strip() for l in inputfile ]

n_digits = len(report[0])
print(f"We have {n_digits} digits per item in the report")

s_gamma = ''
s_epsilon = ''

for digit in range(n_digits):
    half = len(report)/2
    n_ones = 0;
    for item in report:
        if item[digit] == '1':
            n_ones += 1

    print(f"There are {n_ones} 1 digits in position {digit}")
    if n_ones >= half:
        s_gamma += '1'
        s_epsilon += '0'
    else:
        s_gamma += '0'
        s_epsilon += '1'

print(f"Gamma raterate  is {s_gamma}, dec {int(s_gamma,2)}")
print(f"Epsilon rate is {s_epsilon}, dec {int(s_epsilon,2)}")
print(f"Power consumption is { int(s_gamma,2) * int(s_epsilon,2) }")
