#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

inputfile = open( args.inputfile )
report = [ l.strip() for l in inputfile ]

n_digits = len(report[0])
print(f"We have {n_digits} digits per item in the report")

lifesupport = 1
for rating in [ 'Oxygen generator', 'CO2 scrubber' ]:
    keep = report

    for digit in range(n_digits):
        # This time, after each digit, we cut down the length of the report
        half = len(keep)/2
        n_ones = 0;
        for item in keep:
            if item[digit] == '1':
                n_ones += 1

        print(f"There are {n_ones}no '1'-digits of {len(keep)} items in position {digit}")

        # For the oxygen generator rating, we care about the majority
        majority = '0'
        minority = '1'
        if n_ones >= half:
            majority = '1'
            minority = '0'

        if rating == 'Oxygen generator':
            print(f"The majority of digits in position {digit} are {majority}; keep only these ones")
            keep = [ item for item in keep if item[digit] == majority ]
        else:
            print(f"The minority of digits in position {digit} are {minority}; keep only these ones")
            keep = [ item for item in keep if item[digit] == minority ]

        if len(keep) < 2:
            break

    assert(len(keep) == 1)

    print(f"{rating} rating is {keep[0]} aka {int(keep[0],2)}")

    lifesupport *= int(keep[0],2)

print(f"Life support rating is {lifesupport}")
