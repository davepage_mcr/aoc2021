#!/usr/bin/env python3

import argparse
import numpy

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

#######################
## Logic starts here ##
#######################

# Our input is a 2x2 array of heights

rows = []

inputfile = open( args.inputfile )
for line in inputfile:
    rows.append( line.rstrip() )

def nolower(r, c):
    d = int(rows[r][c])

    # Look up
    if r > 0:
        ad = int(rows[r-1][c])
        if ad <= d:
            return False
    if r < len(rows) - 1:
        ad = int(rows[r+1][c])
        if ad <= d:
            return False
    if c > 0:
        ad = int(rows[r][c-1])
        if ad <= d:
            return False
    if c < len(rows[r]) - 1:
        ad = int(rows[r][c+1])
        if ad <= d:
            return False

    return True

risk = 0
for row in range(len(rows)):
    for col in range(len(rows[row])):
        if nolower(row, col):
            print(f"Found a low point at row {row} column {col}, depth {rows[row][col]}")
            risk += int(rows[row][col]) + 1

print(f"Combined risk level: {risk}")
