#!/usr/bin/env python3

import argparse
import pprint
from termcolor import colored

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

pp = pprint.PrettyPrinter()

#######################
## Logic starts here ##
#######################

# Our input is a 2x2 array of heights
# Model this as: [row][col] = [ height, seen ]

rows = []

inputfile = open( args.inputfile )
for line in inputfile:
    rows.append( list( [ int(x), False ] for x in line.rstrip() ) )

# Basically we start at [0,0], and try flood filling until we hit 9s, flipping seen to True

def findbasin(r, c):
    size = flood(r,c)
    print(f"Found a basin of size {size} at row {r} column {c}")
    return size

def flood(r,c):
    flooded = 0

    if rows[r][c][1] == True:
        # We've been here before, abort
        return 0

    if rows[r][c][0] == 9:
        # We've hit an edge, abort
        return 0

    rows[r][c][1] = True
    flooded += 1

    # print(f"Flooding at row {r} column {c}")

    if r > 0:                   # Flood up
        flooded += flood(r-1,c)
    if r < len(rows)-1:         # Flood down
        flooded += flood(r+1,c)
    if c > 0:                   # Flood left
        flooded += flood(r, c-1)
    if c < len(rows[r])-1:      # Flood right
        flooded += flood(r, c+1)

    return flooded

basins = []
for row in range(len(rows)):
    for col in range(len(rows[row])):
        if rows[row][col][0] < 9 and rows[row][col][1] == False:
            basins.append( findbasin(row, col) )

# Fancy print our map and bold places we've seen
for row in range(len(rows)):
    for col in range(len(rows[row])):
        if rows[row][col][1] == True:
            text = colored( rows[row][col][0], 'red' )
        else:
            text = rows[row][col][0]
        print(text, end='')
    print("")

product = 1
for b in sorted(basins)[-3:]:
    product *= b

print(f"3 largest basins product is {product}")

