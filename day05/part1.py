#!/usr/bin/env python3

import argparse
import numpy

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

#######################
## Logic starts here ##
#######################

inputfile = open( args.inputfile )
ventlines = []

max_x = 0
max_y = 0

for line in inputfile:
    coords = line.rstrip().split()
    ( x1, y1 ) = [ int(i) for i in coords[0].split(',') ]
    ( x2, y2 ) = [ int(i) for i in coords[2].split(',') ]

    ventlines.append( [ [x1, y1], [x2, y2] ] )

    if x1 > max_x:
        max_x = x1
    if x2 > max_x:
        max_x = x2

    if y1 > max_y:
        max_y = y1
    if y2 > max_y:
        max_y = y2

seamap = numpy.zeros((max_y+1,max_x+1), dtype="int")

danger_zones = 0

for ventline in ventlines:
    if ventline[0][0] == ventline[1][0]:
        print(f"{ventline}: same x coordinate; vertical line")

        if ventline[0][1] > ventline[1][1]:
            y_direction = -1
        else:
            y_direction = 1

        x = ventline[0][0]
        for y in range(ventline[0][1], ventline[1][1]+1, y_direction):
            seamap[y][x] += 1
            # If this is now 2, it's a danger zone (might increase later; we don't care)
            if seamap[y][x] == 2:
                danger_zones += 1

    elif ventline[0][1] == ventline[1][1]:
        print(f"{ventline}: same y coordinate; horizontal line")

        if ventline[0][0] > ventline[1][0]:
            x_direction = -1
        else:
            x_direction = 1

        y = ventline[0][1]
        for x in range(ventline[0][0], ventline[1][0]+1, x_direction):
            seamap[y][x] += 1
            # If this is now 2, it's a danger zone (might increase later; we don't care)
            if seamap[y][x] == 2:
                danger_zones += 1

    else:
        print(f"{ventline}: Neither horizontal nor vertical")
        continue

print(seamap)
print(f"{danger_zones} danger zones")
