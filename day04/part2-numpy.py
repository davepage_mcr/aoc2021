#!/usr/bin/env python3

import argparse
import pprint
import numpy

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

pp = pprint.PrettyPrinter()

class BingoCard:
    def __init__(self, cardnum, carddata):
        # Carddata is a 2D array containing a tuple of [number, called]
        # called is a boolean which tells us whether that number has been called
        self.cardnum = cardnum
        self.carddata = numpy.array(carddata, dtype="int")

    def mark(self, call_number):
        # Search through our card data to see if we have call_number
        # if we do, mark it as called
        marked = False
        for row in self.carddata:
            for cell in row:
                if cell[0] == call_number:
                    cell[1] = True
                    marked = True
        return marked

    def bingo(self):
        # Search through our card data to see if we have any rows or columns where all numbers are marked

        for n_row in range(self.carddata.shape[0]):
            row = self.carddata[n_row,:]
            markednumbers = [ x for x in row if x[1] == True ]
            if len(markednumbers) == len(row):
                return True

        for n_col in range(self.carddata.shape[1]):
            col = self.carddata[:,n_col]
            markednumbers = [ x for x in col if x[1] == True ]
            if len(markednumbers) == len(col):
                return True

        return False

    def unmarked_sum(self):
        # Returns the sum of all unmarked numbers on the board
        score = 0
        for row in self.carddata:
            for cell in row:
                if cell[1] == False:
                    score += cell[0]
        return score


#######################
## Logic starts here ##
#######################

inputfile = open( args.inputfile )
call_numbers = [ int(x) for x in inputfile.readline().rstrip().split(',') ]

# Skip the blank line after the call numbers
inputfile.readline()

cards = set()
cardnum = 0

carddata = []
for line in inputfile:
    numbers = [ int(x) for x in line.rstrip().split() ]
    if len(numbers) > 0:
        carddata.append([ [ int(x), False ] for x in line.rstrip().split() ])
    else:
        cards.add( BingoCard(cardnum, carddata) )
        cardnum += 1
        carddata = []

cards.add( BingoCard(cardnum, carddata) )

for call_number in call_numbers:
    print(f"*** {call_number} called")

    winning_cards = []

    for card in cards:
        if card.mark(call_number):
            if card.bingo():
                print(f"Card {card.cardnum} calls bingo!")
                winning_cards.append(card)
                if len(cards) == 1:
                    print("This is the last remaining card")
                    print( card.unmarked_sum() * call_number )

    for wcard in winning_cards:
        cards.remove(wcard)

    if len(cards) < 1:
        break
