#!/usr/bin/env python3

import argparse
import pprint

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

pp = pprint.PrettyPrinter()

class BingoCard:
    def __init__(self, cardnum, carddata):
        # Carddata is a 2D array containing a tuple of [number, called]
        # called is a boolean which tells us whether that number has been called
        self.cardnum = cardnum
        self.carddata = carddata

    def mark(self, call_number):
        # Search through our card data to see if we have call_number
        # if we do, mark it as called
        marked = False
        for row in range(len(self.carddata)):
            for col in range(len(self.carddata[0])):
                if self.carddata[row][col][0] == call_number:
                    self.carddata[row][col][1] = True
                    marked = True
        return marked

    def bingo(self):
        # Search through our card data to see if we have any rows or columns where all numbers are marked

        # Check rows because they're easy
        for row in self.carddata:
            markednumbers = [ x for x in row if x[1] == True ]
            if len(markednumbers) == len(row):
                return True

        # Now check columns, which are more of a pain
        for n_col in range(len(self.carddata[0])):
            marked_in_column = 0
            for n_row in range(len(self.carddata)):
                if self.carddata[n_row][n_col][1] == True:
                    marked_in_column += 1
            if marked_in_column == len(self.carddata[0]):
                return True

        return False

    def unmarked_sum(self):
        # Returns the sum of all unmarked numbers on the board
        score = 0
        for row in range(len(self.carddata)):
            for col in range(len(self.carddata[0])):
                if self.carddata[row][col][1] == False:
                    score += self.carddata[row][col][0]
        return score


#######################
## Logic starts here ##
#######################

inputfile = open( args.inputfile )
call_numbers = [ int(x) for x in inputfile.readline().rstrip().split(',') ]

# Skip the blank line after the call numbers
inputfile.readline()

cards = []
cardnum = 0

carddata = []
for line in inputfile:
    numbers = [ int(x) for x in line.rstrip().split() ]
    if len(numbers) > 0:
        carddata.append([ [ int(x), False ] for x in line.rstrip().split() ])
    else:
        cards.append( BingoCard(cardnum, carddata) )
        cardnum += 1
        carddata = []

cards.append( BingoCard(cardnum, carddata) )

for call_number in call_numbers:
    print(f"*** {call_number} called")

    bingocard = None

    for card in cards:
        if card.mark(call_number):
            print(f"Card {card.cardnum}: marked {call_number}")
            if card.bingo():
                print("Bingo!")
                bingocard = card
                break

    if bingocard != None:
        break

print(bingocard.unmarked_sum() * call_number)
