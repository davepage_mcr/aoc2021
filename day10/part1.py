#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

def findcorruption( text ):
    closures = []
    for c in line:
        # First the openers
        if c == '(':
            closures.append(')')
        elif c == '[':
            closures.append(']')
        elif c == '<':
            closures.append('>')
        elif c == '{':
            closures.append('}')
        # Now the closers
        elif c == ')' or c == ']' or c == '>' or c == '}':
            if c == closures[-1]:
                closures.pop()
            else:
                # print(f"Reached closer {c}, expecting {closures[-1]} from {closures}")
                return c
        elif c == '\n':
            continue
        else:
            print(f"Unexpected character {c} in {text}")
            return c

####################
## Main loop here ##
####################

inputfile = open( args.inputfile )

score = 0
for line in inputfile:
    badchar = findcorruption( line.rstrip() )
    if badchar == None:
        continue

    print(f"Line {line.rstrip()} corrupted by {badchar}")
    if badchar == ')':
        score += 3
    elif badchar == ']':
        score += 57
    elif badchar == '}':
        score += 1197
    elif badchar == '>':
        score += 25137

    print(f"Total score: {score}")
