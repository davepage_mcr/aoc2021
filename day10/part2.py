#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

def findclosure( text ):
    # Return None if the line is corrupt
    # Return an empty list if the line is complete
    # Otherwise return a list of symbols to complete
    closures = []
    for c in line:
        # First the openers
        if c == '(':
            closures.append(')')
        elif c == '[':
            closures.append(']')
        elif c == '<':
            closures.append('>')
        elif c == '{':
            closures.append('}')
        # Now the closers
        elif c == ')' or c == ']' or c == '>' or c == '}':
            if c == closures[-1]:
                closures.pop()
            else:
                return None
        elif c == '\n':
            continue
        else:
            raise Exception(f"Unexpected character {c} in {text}")
            return None

    closures.reverse()
    return ''.join(closures)

####################
## Main loop here ##
####################

scores = []

inputfile = open( args.inputfile )
for line in inputfile:
    result = findclosure( line.rstrip() )
    if result == None:  # Ignore corrupt lines
        continue

    score = 0
    for c in result:
        score *= 5
        if c == ')':
            score += 1
        elif c == ']':
            score += 2
        elif c == '}':
            score += 3
        elif c == '>':
            score += 4
        else:
            raise Exception(f"Bad char {c} in {result}")

    print(f"{line.rstrip()} is completed by {result} for score {score}")
    scores.append(score)

print(f"Middle score is {sorted(scores)[ int(len(scores) / 2) ]}")
