#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

olddepth = None
increases = 0

inputfile = open( args.inputfile )
for line in inputfile:
    depth = int(line)

    if olddepth != None and olddepth < depth:
        increases+=1

    olddepth = depth

print(f'Increased depth {increases} times')
