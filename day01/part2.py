#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputfile",  help="Input file of integers")
args = parser.parse_args()

window = []
olddepth = None
increases = 0

inputfile = open( args.inputfile )
for line in inputfile:
    window.append(int(line))
    if len(window) > 3:
        window.pop(0)
    if len(window) < 3:
        continue
    depth = sum(window)

    if olddepth != None and olddepth < depth:
        increases+=1

    olddepth = depth

print(f'Increased depth {increases} times')
